import sys
import os
import numpy as np
import base64
import json
import cv2
import time
import datetime
from app.utils import detector_utils

from multiprocessing.pool import ThreadPool
pool = ThreadPool(processes=5)

def predict_image(request, img):
    yolov5_inference = request.app.yolov5_inference
    date_time = datetime.datetime.now()
    print(f'PREDICT DATE: {date_time}')
    image = base64.b64decode(img.img)
    npimg = np.frombuffer(image, np.uint8)
    image = cv2.imdecode(npimg,cv2.IMREAD_COLOR)
    height, width,_ = image.shape
    result = yolov5_inference.detect(image,img_size=(640, 640),conf_thres=0.25,iou_thres=0.45,max_det=2)
 
    return result
    



    


    
    

