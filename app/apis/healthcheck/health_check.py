import numpy as np
import cv2

def healthChecking(request):
    print("health checking..")
    image = np.random.randint(255, size=(100,100,3),dtype=np.uint8)
    request.app.yolov5_inference.detect(image)
    return {"message":"THE API IS HEALTHY."}