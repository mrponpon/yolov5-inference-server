from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from app.routes import views
from app.utils.class_models import Yolov5_pytorch
import os
import glob
import urllib3
urllib3.disable_warnings()
app = FastAPI()
print(os.getcwd())

@app.on_event("startup")
def compiling_models():
    file_list = []
    for file in glob.glob(f"../weight/*.pt"):
        file_list.append(file)
    app.yolov5_inference = Yolov5_pytorch(file_list[0],names=['dog','cat'],device="")

# Set all CORS enabled origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(views.router)