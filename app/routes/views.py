from fastapi import APIRouter, Depends, Request
from app.apis.predict.predict_ai import predict_image
from app.apis.healthcheck.health_check import healthChecking
from pydantic import BaseModel
from starlette.responses import JSONResponse

class Item(BaseModel):
    img: str

router = APIRouter()

@router.get("/")
def index():
    return "HELLO OCR API"

@router.get("/api/v1/health-check")
def view_1(request: Request):
    return JSONResponse(content = healthChecking(request), status_code = 200)

@router.post("/api/v1/predict", tags=["api_predict"])
def view_2(request: Request,img: Item):
    return JSONResponse(content = predict_image(request,img), status_code = 200)