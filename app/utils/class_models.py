# from app.utils import label_map_util_lite
from app.utils import detector_utils
from app.utils.export import run
from app.utils.augmentations import letterbox
from app.utils.general  import check_img_size ,non_max_suppression,scale_boxes,image2base64
from app.models.common import DetectMultiBackend
import torch
import torch.nn as nn
import os
import numpy as np
import cv2
import glob


class Yolov5_pytorch():
    def __init__(self,weights,names,device):
        self.colors = [40, 0, 255]
        self.names = names
        # Load model
        self.device = detector_utils.select_device(device)
        # run(weights=weights,include=['torchscript'])
        self.model = DetectMultiBackend(weights,device=self.device)

    def detect(self,im,img_size=(640, 640),conf_thres=0.25,iou_thres=0.45,max_det=2,crop_img=False,draw_boxes=False):
        im0 = im.copy()
        im = letterbox(im,img_size, stride=self.model.stride, auto=self.model.pt)[0]  # padded resize
        im = im.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
        im = np.ascontiguousarray(im)  # contiguous
        im = torch.from_numpy(im).to(self.model.device)
        im = im.half() if self.model.fp16 else im.float()  # uint8 to fp16/32
        im /= 255  # 0 - 255 to 0.0 - 1.0
        if len(im.shape) == 3:
            im = im[None]  # expand for batch dim
        if draw_boxes:
            image_src = im0.copy()
            h, w = image_src.shape[:2]
            color = self.colors
        pred = self.model(im, augment=True)
        pred = non_max_suppression(pred,conf_thres,iou_thres, None, False, max_det= max_det)
        predict_result = {}
        for n in self.names:
            predict_result[n] = []
        for i, det in enumerate(pred):  # detections per image
            gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
            if det is not None and len(det):
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_boxes(im.shape[2:], det[:, :4], im0.shape).round()
                # Write results
                for *xyxy, conf, cls in det:
                    x1, y1, x2, y2 = map(int, xyxy)
                    score = float('%.2f' % (conf)) *100
                    name = self.names[int(cls)]
                    temp_result = {
                            "ymin":y1,
                            "ymax":y2,
                            "xmin":x1,
                            "xmax":x2,
                            "score":score,
                            }
                    if crop_img:
                        crop_img = im0[ymin:ymax,xmin:xmax ]
                        base64_crop_img = image2base64(crop_img)
                        temp_result["crop_image"] = base64_crop_img
                    if draw_boxes:
                        tl = 2
                        color = [np.random.randint(0, 255), 0, np.random.randint(0, 255)]
                        cv2.rectangle(image_src, (x1, y1), (x2, y2), color, thickness=max(
                            int((w + h) / 600), 1), lineType=cv2.LINE_AA)
                        label = '%s %.2f' % (name, score)
                        t_size = cv2.getTextSize(
                            label, 0, fontScale=tl / 3, thickness=1)[0]
                        c2 = x1 + t_size[0] + 3, y1 - t_size[1] - 5
                        cv2.rectangle(image_src, (x1 - 1, y1), c2,color, cv2.FILLED, cv2.LINE_AA)
                        t_size = cv2.getTextSize(
                            label, 0, fontScale=tl/ 3, thickness=1)[0]
                        cv2.putText(image_src, label, (x1 + 3, y1 - 4), 0,tl/ 3, [255, 255, 255],
                                    thickness=1, lineType=cv2.LINE_AA)
                    predict_result[name].append(temp_result)
            if draw_boxes:
                predict_result["predict_image"] = image2base64(image_src)
        return predict_result

